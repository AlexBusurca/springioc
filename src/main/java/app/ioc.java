package app;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import service.CustomerService;

@SpringBootApplication
public class ioc {
    public static void main(String[] args) {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");

        CustomerService service = context.getBean(CustomerService.class);

        System.out.println(service.showCustomerDetails());

        context.close();
    }
}
