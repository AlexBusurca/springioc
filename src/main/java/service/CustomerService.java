package service;

import bean.CustomerType;

public class CustomerService {
    CustomerType customerType;

    public void setCustomerType(CustomerType type) {
        this.customerType = type;
    }

    public String showCustomerDetails() {
        return customerType.customerType();
    }
}
